//import mysql
var mysql = require("mysql");

//handles the different routes
function REST_ROUTER(router, connection) {
    var self = this;
    self.handleRoutes(router, connection);
}

REST_ROUTER.prototype.handleRoutes = function(router, connection) {
    // default get
    router.get("/", function(req, res){
        res.json({"Message": "Hello World!"});
    });
    
    // post into database
    router.post("/msg", function(req, res){
        // build query
        //
        var query = "insert into ??(??) values (?)";
        var table = ["msg", "msg", req.body.msg];
        query = mysql.format(query, table);
        connection.query(query, function(err, rows){
            if (err) {
                res.json({"Error": true, "Message": "Error executing Mysql query"});
            } else {
                res.json({"Error": false, "Message": "msg added!"});
            }
        });
    });
    
    // get all messages
    // this needs to be restricted to prevent people from using it to DDOS
    router.get("/grab_all", function(req, res){
        var query = "SELECT * FROM ??";
        var table = ["msg"];
        query = mysql.format(query, table);
        connection.query(query, function(err, rows){
            if (err) {
                res.json({"Error": true, "Message": "Error executing Mysql query"});
            } else {
                res.json({"Error": false, "Message": "Success", "Messages": rows});
            }
        });
    });
    
    // get from database
    router.get("/grab", function(req, res){
        var query = "SELECT * FROM ?? WHERE ??=?";
        var table = ["msg", "msg_id", req.body.msg_id];
        query = mysql.format(query, table);
        connection.query(query, function(err, rows){
            if (err) {
                res.json({"Error": true, "Message": "Error executing Mysql query"});
            } else {
                res.json({"Error": false, "Message": "Success", "Messages": rows});
            }
        });
    });

    // update value already in database
    router.put("/msg", function(req, res){
        var query = "UPDATE ?? SET ?? = ? WHERE ?? = ?";
        var table = ["msg", "msg", req.body.msg, "msg_id", req.body.msg_id];
        query = mysql.format(query, table);
        connection.query(query, function(err, rows){
            if(err) {
                res.json({"Error": true, "Message": "Error executing MySQL query"});
            } else {
                res.json({"Error": false, "Message": "Updated msg_id " + req.body.msg_id});
            }
        });
    });

    // delete value alread in database
    router.delete("/msg", function(req, res){
        var query = "DELETE FROM ?? WHERE ??=?";
        var table = ["msg", "msg_id", req.body.msg_id];
        query = mysql.format(query, table);
        connection.query(query, function(err, rows){
            if(err) {
                res.json({"Error": true, "Message": "Error executing MySQL query"});
            } else {
                res.json({"Error": false, "Message": "Deleted msg_id " + req.body.msg_id});
            }
        });
    });
}

module.exports = REST_ROUTER;
