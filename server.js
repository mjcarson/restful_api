// import packages
var express = require('express');
var mysql = require('mysql');
var md5 = require('MD5');
var bodyParser = require('body-parser');
var rest = require('./REST.js');
var cluster = require('cluster');
var app = express();

// port to bind to
const port = 8001;

// setup cluster
if (cluster.isMaster){
    // set up master
    console.log('[*] Listening on port ' + port);
    var numWorkers = require('os').cpus().length;
    console.log('[*] Master cluster setting up ' + numWorkers + ' workers');
    for (var i = 0; i < numWorkers; i++){
        cluster.fork();
    }

    // spawn workers
    cluster.on('online', function(worker) {
        console.log('[*] Worker ' + worker.process.pid + ' is online');
    });

    // handler worker death
    cluster.on('exit', function(worker, code, signal) {
        console.log('[!] Worker ' + worker.process.pid + ' died with code: ' + 
            code + ', and signal: ' +  signal);
        console.log('[*] Starting a new worker');
        cluster.fork();
    });
} else {

    function REST(){
        this.connectMysql();
    };

    REST.prototype.connectMysql = function() {
        var self = this;
        // connect to mysql
        var pool = mysql.createPool({
            connectionLimit : 100,
            host : '127.0.0.1',
            user : 'api',
            password : 'password1!',
            database : 'restful_api',
            debug : false
        });

        // grab a connection
        pool.getConnection(function(err, connection){
            if (err) {
                self.stop(err);
            } else {
                self.configureExpress(connection);
            }
        });
    }

    // setup express
    REST.prototype.configureExpress = function(connection){
        var self = this;
        app.use(bodyParser.urlencoded({extended: true}));
        app.use(bodyParser.json());
        var router = express.Router();
        app.use('/api', router);
        var rest_router = new rest(router, connection, md5);
        self.startServer();
    }

    // start server
    REST.prototype.startServer = function() {
        app.listen(port, function() {});
    }

    // stop server
    REST.prototype.stop = function(err) {
        console.log("ISSUE WITH MYSQL" + err);
        process.exit(1);
    }

    new REST();
}
